module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    // "plugin:prettier/recommended",
    "plugin:vue/vue3-recommended",
    "plugin:nuxt/base",
  ],
  plugins: [],
  rules: {
    semi: [1, "always"],
  },
};
